import './styles.scss'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { IApplicationState, selectErrorsData } from '@store/root.reducer'
import { Dispatch } from 'redux'
import { LoginAction } from '@store/auth/auth.actions'
import { Input } from '@components/Input/Input'
import withReduxSaga from '../../../index'
import Link from 'next/link'
import { Button } from '@components/Button/Button'
// import { Checkbox } from '@components/Checkbox/Checkbox'

interface IConnectedState {
  readonly error: any
}

interface IConnectedDispatch {
  login: (params: any) => void
}

type IProps = IConnectedDispatch & IConnectedState

const mapStateToProps = (state: IApplicationState): IConnectedState => ({
  error: selectErrorsData(state),
})

const mapDispatchToProps = (dispatch: Dispatch): IConnectedDispatch => ({
  login: (data: any) => dispatch(LoginAction(data)),
})

interface IState {
  [key: string]: any
  username: string
  password: string
}

class Login extends Component<IProps, IState> {
  public state = {
    username: '',
    password: '',
  }

  public showErrorMsg = (fieldName: any) => {
    const { error } = this.props
    if (error && error[fieldName]) {
      return error[fieldName][0]
    }
  }

  public loginHandler = (data: any) => {
    this.props.login(data)
  }

  public handleChange = (event: any) => {
    const { name, value } = event.target
    this.setState({
      [name]: value,
    })
  }

  public submit = () => {
    const { username, password } = this.state
    const credentials = {
      username,
      password,
    }

    this.props.login(credentials)
  }

  // public handleNewsCheckbox = (event: React.SyntheticEvent<any>) => {
  //   this.setState({
  //       ...this.state.userData,
  //       receive_newsletter: (event.target as any).checked,
  //
  //   })
  // }

  public render() {
    const { username, password } = this.state

    return (
        <div className="login-wrapper">
          <h2 className="modal__title">log in to hockey share</h2>
          <Input
            placeholder="Username"
            name="username"
            type="text"
            value={username}
            onChange={this.handleChange}
            error={this.showErrorMsg('username')}
            errorMessage={this.showErrorMsg('username')}
          />

          <Input
            placeholder="Password"
            name="password"
            type="password"
            value={password}
            onChange={this.handleChange}
            error={this.showErrorMsg('password')}
            errorMessage={this.showErrorMsg('password')}
          />

          {/*<div className="agreements-item">*/}
          {/*<span className="d-block modal__text-color">Newsletter:</span>*/}
          {/*<Checkbox*/}
          {/*onChange={e => this.handleNewsCheckbox(e)}*/}
          {/*checked={receive_newsletter}*/}
          {/*/>*/}
          {/*</div>*/}

          <div className="forgot-link">
            <Link href={`/auth/recovery`}>
              <a>Forgot Password</a>
            </Link>
          </div>

          <div className="login-wrapper__sub-btns-block">
            <div className="light-link">
              <Link href={`/auth/register`}>
                <a> Create a new account </a>
              </Link>
            </div>
            <Button onClick={this.submit} label="log in" />
          </div>

        </div>

    )
  }
}

export default withReduxSaga(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login)
)
