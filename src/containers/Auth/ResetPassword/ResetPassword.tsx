import { useRouter } from 'next/router'
import Link from 'next/link'
import React, { useState } from 'react'
import { Input } from '@components/Input/Input'
import { Button } from '@components/Button/Button'
import { useDispatch, useSelector } from 'react-redux'
import {
  IApplicationState,
  selectErrorsData,
  selectResetPasswordStatus,
} from '@store/root.reducer'
import { ResetPasswordAction } from '@store/auth/auth.actions'

const ResetPassword = () => {
  const [password, setPassword] = useState('')
  const [password_confirmation, setPasswordConfirmation] = useState('')
  const [passwordMatch, setPasswordMatch] = useState(true)

  const dispatch = useDispatch()

  const {
    query: { token, email },
  } = useRouter()

  const error = useSelector((state: IApplicationState) =>
    selectErrorsData(state)
  )
  const status = useSelector((state: IApplicationState) =>
    selectResetPasswordStatus(state)
  )

  const showErrorMsg = (fieldName: any) => {
    if (error && error[fieldName]) {
      return error[fieldName][0]
    }
  }

  const submit = () => {
    const data = {
      email,
      token,
      password,
      password_confirmation,
    }

    if (password_confirmation !== password) {
      setPasswordMatch(false)
      return
    } else {
      setPasswordMatch(true)
    }

    dispatch(ResetPasswordAction(data))
  }

  const successMessage = () => {
    return (
      <div>
        <h3>
          Password successfully updated. You may now log in with your new
          password.
        </h3>
        <br/>
        <div className="light-link">
        <Link href={`/auth/login`}>
          <a className="">click here to log in</a>
        </Link>
        </div>
      </div>
    )
  }

  const renderInfo = () => {
    return (
      <div>
        <h1>Create new password</h1>
        <br/>
        {status}
        <Input
          placeholder="New Password"
          name="password"
          type="password"
          value={password}
          onChange={(e: React.SyntheticEvent) =>
            setPassword((e.target as HTMLInputElement).value)
          }
          error={showErrorMsg('password')}
          errorMessage={showErrorMsg('password')}
        />

        <Input
          placeholder="Confirm New Password"
          name="passwordConfirm"
          type="password"
          value={password_confirmation}
          onChange={(e: React.SyntheticEvent) =>
            setPasswordConfirmation((e.target as HTMLInputElement).value)
          }
          error={!passwordMatch}
          errorMessage="The password confirmation does not match."
        />
        <Button label="send reset information" onClick={submit} />
      </div>
    )
  }

  return <div>{status ? successMessage() : renderInfo()}</div>
}

export default ResetPassword
