import { useRouter } from 'next/router'
import Link from 'next/link'
import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { EmailVerifyAction } from '@store/auth/auth.actions'

const Token = () => {
  const dispatch = useDispatch()

  const {
    query: { token },
  } = useRouter()

  useEffect(() => {
    dispatch(EmailVerifyAction(token))
  }, [])


  return (
    <div>
      <h1>Your account is now ready to use!</h1>
      <br/>
      <p>
        Thanks for confirming your account. You can now log in to your account
        using your credentials.
      </p>
      <br/>
      <div className="light-link">
      <Link href={`/auth/login`}>
        <a className="">click here to log in</a>
      </Link>
      </div>

    </div>
  )
}

export default Token
