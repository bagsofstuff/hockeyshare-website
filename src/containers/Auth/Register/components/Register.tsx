import './register.scss'
import React, {Component} from 'react'
import classnames from 'classnames'
import Link from 'next/link'
import {Input} from '@components/Input/Input'
import {SelectField} from '@components/SelectField/SelectField'
import {Checkbox} from '@components/Checkbox/Checkbox'
import {Button} from '@components/Button/Button'

interface IState {
  [key: string]: any

  userData: {
    email: string
    username: string
    password: string
    position_id: number
    first_name: string
    last_name: string
    password_confirmation: any
    receive_newsletter: boolean
  }
  agreements: Set<string>
  agreementError: boolean
  ageError: boolean
}

class Register extends Component<any, IState> {
  public state = {
    userData: {
      email: '',
      username: '',
      password: '',
      position_id: 0,
      first_name: '',
      last_name: '',
      password_confirmation: '',
      receive_newsletter: false,
    },
    agreements: new Set<string>(),
    agreementError: false,
    ageError: false,
    passwordMatch: true,
  }

  public showErrorMsg = (fieldName: any) => {
    const {error} = this.props
    if (error && error[fieldName]) {
      return error[fieldName][0]
    }
  }

  public userPositionsData = () => {
    const {userPositions} = this.props
    return userPositions.map((pos: any) => {
      return {
        label: pos.name,
        value: pos.id,
      }
    })
  }

  public handleChange = (event: any, selectName?: string) => {
    if (selectName) {
      this.setState({
        userData: {
          ...this.state.userData,
          [selectName]: event.value,
        },
      })
      return
    }

    const {name, value} = event.target
    this.setState({
      userData: {
        ...this.state.userData,
        [name]: value,
      },
    })
  }

  public checkAgreementsErrors = () => {
    const values = Array.from(this.state.agreements.values())
    const fields = [
      {
        field: 'age',
        err: 'ageError',
      },
      {
        field: 'agreement',
        err: 'agreementError',
      },
    ]
    fields.forEach(item => {
      if (values.indexOf(item.field) === -1) {
        this.setState({
          [item.err]: true,
        })
      } else {
        this.setState({
          [item.err]: false,
        })
      }
    })
  }

  public submit = async () => {
    await this.checkAgreementsErrors()
    const {userData, ageError, agreementError} = this.state

    if (userData.password_confirmation !== userData.password) {
      this.setState({passwordMatch: false})
      return
    } else {
      this.setState({passwordMatch: true})
    }

    const copy = {...userData}

    if (userData.last_name.trim() === '') {
      delete copy.last_name
    }

    if (ageError || agreementError) {
      return
    }
    this.props.registerHandler(copy)
  }

  public onChangeType = (item: string) => {
    const prevState: Set<string> = new Set<string>(this.state.agreements)
    this.toggleItem(prevState, item)
    this.setState({agreements: prevState})
  }

  public toggleItem = (source: Set<string>, item: string) =>
    source.has(item) ? source.delete(item) : source.add(item)

  public checkedType = (value: string) =>
    Boolean(this.state.agreements.has(value))

  public handleNewsCheckbox = (event: React.SyntheticEvent<any>) => {
    this.setState({
      userData: {
        ...this.state.userData,
        receive_newsletter: (event.target as any).checked,
      },
    })
  }

  public render() {
    const {
      userData: {
        email,
        username,
        password,
        first_name,
        last_name,
        password_confirmation,
        receive_newsletter,
      },
      ageError,
      agreementError,
      passwordMatch,
    } = this.state

    return (
      <div className="register__wrapper">
        <h2 className="modal__title">Register</h2>
        <p className="modal__text">
          Want to become a member of one of the fastest growing hockey
          communities? Simply fill out the form below to create your FREE
          account today!
          <span>
            Already have an account?
            <Link href={`/auth/login`}>
              <a className="modal__login-link">Click here to log in.</a>
            </Link>
          </span>
        </p>
        <h3 className="modal__small-title">User Profile</h3>

        <Input
          placeholder="Email"
          name="email"
          type="email"
          value={email}
          onChange={this.handleChange}
          error={this.showErrorMsg('email')}
          errorMessage={this.showErrorMsg('email')}
        />

        <div>
          <Input
            placeholder="Username"
            name="username"
            type="text"
            value={username}
            onChange={this.handleChange}
            // onBlur={this.handleBlur}
            error={this.showErrorMsg('username')}
            errorMessage={this.showErrorMsg('username')}
          />
        </div>

        <Input
          placeholder="Password"
          name="password"
          type="password"
          value={password}
          onChange={this.handleChange}
          error={this.showErrorMsg('password')}
          errorMessage={this.showErrorMsg('password')}
        />

        <Input
          placeholder="Password Confirmation"
          name="password_confirmation"
          type="password"
          value={password_confirmation}
          onChange={this.handleChange}
          error={!passwordMatch}
          errorMessage="The password confirmation does not match."
        />

        <h3 className="modal__small-title">Personal Information</h3>
        <SelectField
          options={this.userPositionsData()}
          onChange={e => this.handleChange(e, 'position_id')}
          error={this.showErrorMsg('position_id')}
        />

        <Input
          placeholder="First Name"
          name="first_name"
          type="text"
          value={first_name}
          onChange={this.handleChange}
          error={this.showErrorMsg('first_name')}
          errorMessage={this.showErrorMsg('first_name')}
        />

        <Input
          placeholder="Last Name"
          name="last_name"
          type="text"
          value={last_name}
          onChange={this.handleChange}
          error={this.showErrorMsg('first_name')}
          errorMessage={this.showErrorMsg('first_name')}
        />
        <h3 className="modal__small-title">Submit Registration </h3>
        <div
          className={classnames('agreements-item', {error: agreementError})}
        >
          <span className="d-block modal__text-color">Agreement:</span>
          <Checkbox
            onChange={() => this.onChangeType('agreement')}
            checked={this.checkedType('agreement')}
          />
          <span className="agreements-item__checkbox-label">I agree to the HockeyShare
            <Link href={`/auth/login`}><a className="">Terms of Service/Privacy Policy</a></Link>
          </span>
        </div>

        <div className={classnames('agreements-item', {error: ageError})}>
          <span className="d-block modal__text-color">Legal Age:</span>
          <Checkbox
            onChange={() => this.onChangeType('age')}
            checked={this.checkedType('age')}
          />
          <span className="agreements-item__checkbox-label">The person completing this form is over the age of 13 years old</span>
        </div>

        <div className="agreements-item">
          <span className="d-block modal__text-color">Newsletter:</span>
          <Checkbox
            onChange={e => this.handleNewsCheckbox(e)}
            checked={receive_newsletter}
          />
          <span className="agreements-item__checkbox-label">Receive monthly emails from HockeyShare </span>
        </div>

        <Button label="Submit" onClick={this.submit}/>
      </div>
    )
  }
}

export default Register
