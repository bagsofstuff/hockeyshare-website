import React from 'react'

const AfterRegister = () => {
  return (
    <div className="register-success">
      <h1>Your registration was successful!</h1>
      <p>
        Thank you for signing up to be part of one of the biggest and fastest
        growing hockey communities on the web. Please keep an eye on your inbox
        for further instructions on how to use your account. The email will be
        sent from info@hockeyshare.com, so if HockeyShare.com isn't on your safe
        sender's list, you may want to check your bulk mail/spam folder. Emails
        are typically delivered in nearly real-time, but on occasion can be
        delayed. If you don't receive your confirmation email, please use the
        contact form to drop us a line so we can get you set up properly.
      </p>

      <div className="img-wrap">
      <img src={require('./check.svg')} alt="LOGO" />
      </div>
    </div>
  )
}

export default AfterRegister
