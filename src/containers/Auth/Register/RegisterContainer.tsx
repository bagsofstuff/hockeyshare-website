import React, { useEffect } from 'react'
import Register from '@containers/Auth/Register/components/Register'
import { useDispatch, useSelector } from 'react-redux'
import AfterRegister from '@containers/Auth/Register/components/AfterRegister'
import {
  IApplicationState,
  selectRegisterRedirectStatus,
  selectErrorsData,
  selectUserPositions,
} from '@store/root.reducer'

import { GetUserPositionsAction } from '@store/user/user.actions'
import { RegisterAction } from '@store/auth/auth.actions'

const RegisterContainer = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(GetUserPositionsAction())
  }, [])

  const registerHandler = (data: any) => {
    dispatch(RegisterAction(data))
  }

  const registerRedirect = useSelector((state: IApplicationState) =>
    selectRegisterRedirectStatus(state)
  )

  const userPositions = useSelector((state: IApplicationState) =>
    selectUserPositions(state)
  )

  const error = useSelector((state: IApplicationState) =>
    selectErrorsData(state)
  )

  return (
    <main>
      {registerRedirect ? (
        <AfterRegister />
      ) : (
        <Register
          userPositions={userPositions}
          error={error}
          registerHandler={registerHandler}
        />
      )}
    </main>
  )
}

export default RegisterContainer
