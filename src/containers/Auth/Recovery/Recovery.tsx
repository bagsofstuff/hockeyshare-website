import './styles.scss'
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ForgotPasswordAction } from '@store/auth/auth.actions'
import { Button } from '@components/Button/Button'
import { Input } from '@components/Input/Input'
import {
  IApplicationState,
  selectErrorsData,
  selectForgotPasswordStatus,
} from '@store/root.reducer'

const Recovery = () => {
  const dispatch = useDispatch()
  const status = useSelector((state: IApplicationState) =>
    selectForgotPasswordStatus(state)
  )
  const error = useSelector((state: IApplicationState) =>
    selectErrorsData(state)
  )

  const [email, setEmail] = useState('')

  const submit = () => dispatch(ForgotPasswordAction(email))

  const successMessage = () => {
    return (
      <h3>
        If an account is associated with the email address specified, you will
        receive an email with reset instructions. Please be sure to check your
        spam / junk folders.
      </h3>
    )
  }

  const showErrorMsg = (fieldName: any) => {
    if (error && error[fieldName]) {
      return error[fieldName][0]
    }
  }

  const renderInfo = () => {
    return (
      <div>
        <h1>Password Recovery</h1>
        <p>
          Can't remember your login? Use the form below to reset your password.
        </p>
        <Input
          placeholder="Email Address:"
          name="email"
          type="email"
          value={email}
          onChange={(e: React.SyntheticEvent) =>
            setEmail((e.target as HTMLInputElement).value)
          }
          error={showErrorMsg('email')}
          errorMessage={showErrorMsg('email')}
        />
        <Button label="send reset information" onClick={submit} />
      </div>
    )
  }

  return <div className="recovery-wrapper">{status ? successMessage() : renderInfo()}</div>
}

export default Recovery
