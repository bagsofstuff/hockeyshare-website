import * as React from 'react'
import './TopBanner.scss'

const TopBanner = () => {
  return (
    <div id="main" className="main">
      <div className="main-block">
        <div className="main-text">
          <h2 className="main__title">national leagues 2020 Season begins</h2>
          <p className="main__subtitle">
            Here are all the important dates in 2020 for the National Hockey
            League.
          </p>
          <div className="main__button">
            <a href="">Read More</a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default TopBanner
