import axios, { AxiosResponse } from 'axios'

import Cookies from 'js-cookie'

axios.defaults.headers.Authorization = 'Bearer ' + (Cookies.get('token') || '')

// axios.interceptors.response.use(
//   (resp) => resp,
//   (error) => {
//     if (error.response.status === 401) {
//       window.location.assign('/auth');
//       localStorage.removeItem('token');
//     }
//     return Promise.reject(error);
//   }
// );

export function get(url: string, data?: any): Promise<AxiosResponse> {
  const params = new URLSearchParams()
  if (typeof data === 'object' && Object.keys(data).length > 0) {
    Object.keys(data).map(key => {
      const value = data[key]
      params.append(key, value)
    })
  }
  return axios.get(process.env.URL + url, { params })
}

export function post(url: string, data?: any): Promise<AxiosResponse> {
  return axios.post(process.env.URL + url, data)
}

export function put(url: string, data?: any): Promise<AxiosResponse> {
  return axios.put(process.env.URL + url, data)
}

export function removeRequest(url: string, data?: any): Promise<AxiosResponse> {
  return axios.delete(process.env.URL + url, { params: data })
}

export function addToken(token?: string) {
  axios.defaults.headers.Authorization = `Bearer ${
    token ? token : Cookies.get('token')
  }`
}

export function deleteHeader(key: string) {
  delete axios.defaults.headers[key]
}
