import React, { Component } from 'react'
class AuthModal extends Component<any, any> {
  public render() {
    if (this.props.activeModals.indexOf('AuthModal') === -1) {
      return ''
    }

    return (
      <div className="modal">
        <div className="modal__body">
          <div
            className="modal__close"
            onClick={() => this.props.closeModalHandler('AuthModal')}
          >
            ×
          </div>
          <div className="modal__content" />
        </div>
      </div>
    )
  }
}

export default AuthModal
