import * as React from 'react'
import classnames from 'classnames'
import './Button.scss'

type ButtonTypes = 'button' | 'submit' | 'reset'

type ButtonColors = 'primary' | 'grey' | 'lightgrey'

interface Props {
  readonly id?: string
  readonly style?: React.CSSProperties
  readonly className?: string
  readonly type?: ButtonTypes
  readonly disabled?: boolean
  readonly color: ButtonColors
  readonly label: React.ReactNode
  readonly itemRef?: any
  onClick?: () => void
}

interface DefaultProps {
  type: ButtonTypes
  color: ButtonColors
  disabled: boolean
}

export class Button extends React.PureComponent<Props, any> {
  public static defaultProps: DefaultProps = {
    type: 'button',
    color: 'primary',
    disabled: false,
  }

  constructor(props: Props) {
    super(props)
  }

  get buttonClassName(): string {
    const { className, color } = this.props
    const buttonColor: string = `tp-btn__${color}`
    return classnames('tp-btn', buttonColor, className)
  }

  get styles() {
    return { ...this.props.style }
  }

  public onClick = (event: any) => {
    if (this.props.disabled) {
      event.preventDefault()
      return
    }

    if (this.props.onClick) {
      this.props.onClick()
    }
  }

  public render() {
    const { id, type, label, itemRef, disabled } = this.props

    return (
      <button
        id={id}
        ref={itemRef}
        type={type}
        className={this.buttonClassName}
        onClick={this.onClick}
        disabled={disabled}
        style={this.styles}
      >
        <span>{label}</span>
      </button>
    )
  }
}
