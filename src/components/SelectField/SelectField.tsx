import * as React from 'react'
import Select from 'react-select'
import './SelectField.scss'
import classnames from 'classnames'

const configureStyles = {
  control: (styles: React.CSSProperties) => ({
    ...styles,
    alignItems: 'center',
    alignContent: 'center',
    borderRadius: 0,
    borderColor: '#607d8b',
    minHeight: 32,
    cursor: 'pointer',
    ':hover': {
      color: '#78909c',
      borderColor: '#b0bec5',
    },
  }),
  option: (styles: React.CSSProperties) => ({
    ...styles,
    color: '#37474f',
  }),
  placeholder: (styles: React.CSSProperties) => ({
    ...styles,
    color: '#37474f',
  }),
  indicatorSeparator: () => ({
    display: 'none',
  }),
  dropdownIndicator: (styles: React.CSSProperties, state: any) => {
    if (state.selectProps.menuIsOpen) {
      return {
        ...styles,
        transform: 'rotate(180deg)',
      }
    }
    return { ...styles }
  },
}

export interface IOption {
  label: string
  value: string | number
  [key: string]: any
}

interface Props {
  readonly id?: string
  readonly name?: string
  readonly className?: string
  readonly placeholder?: string
  readonly options: IOption[] | any
  readonly value?: IOption
  readonly disabled?: boolean
  onChange: (event: any) => void
  error?: boolean
}

export class SelectField extends React.Component<Props, any> {
  constructor(props: Props) {
    super(props)
  }

  get inputClassName(): string {
    const { className, error } = this.props
    return classnames(
      'basic-select',
      {
        'basic-select--invalid': error,
      },
      className
    )
  }

  public render(): React.ReactNode {
    const { id, options, onChange, ...props } = this.props

    return (
      <Select
        {...props}
        id={id}
        instanceId={1}
        className={this.inputClassName}
        classNamePrefix="tp-select"
        options={options}
        onChange={onChange}
        styles={{ ...configureStyles }}
        placeholder={this.props.placeholder}
      />
    )
  }
}
