import * as React from 'react'
import Link from 'next/link'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Cookies from 'js-cookie'

import { faEnvelope, faUser } from '@fortawesome/free-solid-svg-icons'
import {
  faTwitter,
  faFacebookF,
  faYoutube,
} from '@fortawesome/free-brands-svg-icons'

library.add(faEnvelope, faTwitter, faFacebookF, faYoutube, faUser)

class Header extends React.Component<any> {
  public state = {
    isClient: false,
  }

  public componentDidMount() {
    this.setState({
      isClient: true,
    })
  }

  get routes() {
    const icons = [
      {
        path: '/',
        title: 'Home',
      },
      {
        path: '/',
        title: 'drills & practices',
      },
      {
        path: '/',
        title: 'team',
      },
      {
        path: '/',
        title: 'schedule',
      },
      {
        path: '/',
        title: 'store',
      },
      {
        path: '/contact',
        title: 'contact us',
      },
    ]

    return (
      <ul className="menu-list">
        {icons.map((route, idx) => (
          <li key={idx} className="menu__item">
            <Link href={`${route.path}`}>
              <a className="menu__link">{route.title}</a>
            </Link>
          </li>
        ))}
      </ul>
    )
  }

  get profileLink() {
    const { isClient } = this.state
    const token = Cookies.get('token')
    const url = isClient && token ? `/profile` : `/auth/register`
    return (
      <div className="menu-social__item-profile">
        <Link href={url}>
          <a className="menu-social__link-profile ">
            <FontAwesomeIcon icon={faUser} />
          </a>
        </Link>
      </div>
    )
  }

  get socialIcons() {
    const arrIcons = [
      {
        type: <FontAwesomeIcon icon={faEnvelope} />,
        path: '',
      },
      {
        type: <FontAwesomeIcon icon={faFacebookF} />,
        path: '',
      },
      {
        type: <FontAwesomeIcon icon={faTwitter} />,
        path: '',
      },
      {
        type: <FontAwesomeIcon icon={faYoutube} />,
        path: '',
      },
    ]

    return (
      <ul className="menu-social">
        {arrIcons.map((icon, idx) => (
          <li key={idx} className="social menu-social__item">
            <a className="menu-social__link" href={`${icon.path}`}>
              {icon.type}
            </a>
          </li>
        ))}
      </ul>
    )
  }

  public render() {
    return (
      <div>
        <header id="header" className="header-nav">
          <div className="header__logo">
            <Link href={`/`}>
              <img src={require('./logo.png')} alt="LOGO" />
            </Link>
          </div>
          <nav className="menu">{this.routes}</nav>
          <div className="menu-social__wrapper">
            {this.profileLink}
            {this.socialIcons}
          </div>
          <div className="menu__dropdown">
            <span />
            <span />
            <span />
          </div>
        </header>
      </div>
    )
  }
}

export default Header
