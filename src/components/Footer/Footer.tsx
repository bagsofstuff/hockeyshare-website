import * as React from 'react'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import {
  faTwitter,
  faFacebookF,
  faYoutube,
} from '@fortawesome/free-brands-svg-icons'

library.add(faEnvelope, faTwitter, faFacebookF, faYoutube)

const Footer = () => {
  const socialIcons = () => {
    const arrIcons = [
      {
        type: <FontAwesomeIcon icon={faEnvelope} />,
        path: '',
      },
      {
        type: <FontAwesomeIcon icon={faFacebookF} />,
        path: 'https://www.facebook.com/hockeyshare',
      },
      {
        type: <FontAwesomeIcon icon={faTwitter} />,
        path: 'https://twitter.com/hockeyshare',
      },
      {
        type: <FontAwesomeIcon icon={faYoutube} />,
        path: 'https://www.youtube.com/hockeyshare',
      },
    ]

    return (
      <ul className="social-list">
        {arrIcons.map((icon, idx) => (
          <li key={idx} className="social social__item">
            <a target="_blank" className="social__link" href={`${icon.path}`}>
              {icon.type}
            </a>
          </li>
        ))}
      </ul>
    )
  }

  return (
    <footer id="footer" className="footer">
      <div className="footer-block">
        <div className="footer__logo">
          <img src={require('./logo.png')} alt="LOGO" />
        </div>
        <div className="footer-text">
          <div className="footer-text__info">
            <span className="small-title footer-text__title">Info</span>
            <span>mailinfo@ex.com</span>
            <span>987 654 321</span>
          </div>
          <div className="footer-text__address">
            <span className="small-title footer-text__title">Address</span>
            <span>1122 addres Rd, Address,</span>
            <span>Ca 654321</span>
          </div>
        </div>
        <div className="footer-social">{socialIcons()}</div>
      </div>
      {/*/!* Исправить лого на фон в css*!/*/}
      {/*<div className="footer-bg-logo">*/}
      {/*  <img src={require('./Footer-back-logo.png')} alt="" />*/}
      {/*</div>*/}
    </footer>
  )
}

export default Footer
