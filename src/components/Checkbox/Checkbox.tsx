import * as React from 'react'
import classnames from 'classnames'
import './Checkbox.scss'

interface Props {
  id?: string
  className?: string
  style?: React.CSSProperties
  value?: any
  disabled?: boolean
  checked: boolean
  onChange?: (event: React.SyntheticEvent<any>) => void
}

interface DefaultProps {
  checked: boolean
  disabled: boolean
}

type PropsWithDefault = Props & DefaultProps

interface State {
  checked: boolean
}

export class Checkbox extends React.Component<Props, State> {
  public static defaultProps: DefaultProps = {
    checked: false,
    disabled: false,
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      checked: (props as PropsWithDefault).checked,
    }
  }

  public UNSAFE_componentWillReceiveProps(nextProps: Readonly<Props>): void {
    if (this.props.checked !== nextProps.checked) {
      this.setState({ checked: nextProps.checked })
    }
  }

  get checked(): boolean {
    return this.state.checked
  }

  get checkBoxClassName(): string {
    return classnames(
      'tp-checkbox',
      {
        'tp-checkbox--default': !this.checked,
        'tp-checkbox--checked': this.checked,
      },
      this.props.className
    )
  }

  get styles() {
    return { ...this.props.style }
  }

  public handleChange = (event: React.SyntheticEvent<any>): void => {
    if (this.props.disabled) {
      return
    }

    if (this.props.onChange) {
      this.props.onChange(event)
      return
    }

    this.setState({ checked: !this.state.checked })
  }

  public render(): React.ReactNode {
    const { disabled, ...props } = this.props
    return (
      <div className={this.checkBoxClassName} style={this.styles}>
        <input
          {...props}
          type="checkbox"
          checked={this.checked}
          disabled={disabled}
          onChange={this.handleChange}
        />
      </div>
    )
  }
}
