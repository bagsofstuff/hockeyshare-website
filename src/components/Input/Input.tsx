import * as React from 'react'
import classnames from 'classnames'
// import { TextArea } from './TextArea';
import './Input.scss'

type InputType = 'text' | 'number' | 'password' | 'email'

interface Props {
  readonly itemRef?: React.RefObject<any>
  readonly name?: string
  readonly className?: string
  readonly style?: React.CSSProperties
  readonly type?: InputType
  readonly value?: string
  readonly placeholder?: string
  readonly pattern?: RegExp | string
  readonly error?: any
  readonly errorMessage?: string
  readonly maxLength?: number
  readonly rows?: number
  readonly disabled?: boolean
  readonly search?: boolean
  onChange?: (event: React.SyntheticEvent<any>) => void
  onBlur?: () => void
}

export class Input extends React.Component<Props, any> {
  constructor(props: Props) {
    super(props)
  }

  // get multiline(): boolean {
  //   return typeof this.props.rows !== 'undefined';
  // }

  get inputClassName(): string {
    const { className, disabled, error, search } = this.props
    return classnames(
      'tp-input',
      {
        // 'tp-input--multiline': this.multiline,
        'tp-input--invalid': error,
        'tp-input--disabled': disabled,
        'tp-input--search': search,
      },
      className
    )
  }

  get searchBox(): React.ReactNode {
    return <div className="search" />
  }

  public handleChange = (event: React.SyntheticEvent<any>) => {
    if (this.props.onChange) {
      this.props.onChange(event)
    }
  }

  public handleBlur = () => {
    if (this.props.onBlur) {
      this.props.onBlur()
    }
  }

  public render(): React.ReactNode {
    const { itemRef, errorMessage, error, search, ...props } = this.props

    const Component: any = 'input'

    return (
      <div className="input-field">
        {React.createElement<Props>(Component, {
          ...props,
          ref: itemRef,
          className: this.inputClassName,
          onChange: this.handleChange,
          onBlur: this.handleBlur,
        })}
        {error ? <div className="error-message">{errorMessage}</div> : null}
        {search ? this.searchBox : null}
      </div>
    )
  }
}
