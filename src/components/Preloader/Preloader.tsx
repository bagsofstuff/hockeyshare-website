import React from 'react'
import './Preloader.scss'

interface IProps {
  readonly children: any
  readonly loading: boolean
}

export const Loading = () => {
  return (
    <div className="preloader">
      <div className="spinner" />
    </div>
  )
}

export const Preloader = (props: IProps) => {
  const { loading, children } = props
  return loading ? <Loading /> : children
}
