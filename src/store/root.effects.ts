import { all, fork } from 'redux-saga/effects'
import { authSaga } from './auth/auth.effects'
import { userSaga } from './user/user.effects'

export default function* rootSaga() {
  yield all([fork(authSaga), fork(userSaga)])
}
