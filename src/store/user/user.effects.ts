import { put, takeLatest } from '@redux-saga/core/effects'
import {
  ActionTypes,
  GetUserPositionsActionSuccess,
  GetUserPositionsActionFail,
} from './user.actions'
import { get } from '../../utils/httpRequests'

function* getUserPositionsSaga(action: any) {
  try {
    const { data } = yield get('/api/v1/positions', action.payload)
    yield put(GetUserPositionsActionSuccess(data.data))
  } catch (error) {
    yield put(GetUserPositionsActionFail(error.response.data))
  }
}

export function* userSaga() {
  yield takeLatest(ActionTypes.GET_USER_POSITIONS_ACTION, getUserPositionsSaga)
}
