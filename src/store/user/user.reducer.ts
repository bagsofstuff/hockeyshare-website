import { ActionTypes } from './user.actions'
import { IUserState } from '@store/user/user.models'

export const initialState: IUserState = {
  loading: true,
  error: '',
  positionsList: [],
}

export function reducer(
  state: IUserState = initialState,
  action: any
): IUserState {
  switch (action.type) {
    case ActionTypes.GET_USER_POSITIONS_ACTION:
      return { ...state, loading: true, error: '' }
    case ActionTypes.GET_USER_POSITIONS_ACTION_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        positionsList: action.payload,
      }
    case ActionTypes.GET_USER_POSITIONS_ACTION_ERROR:
      return { ...state, loading: false }

    default:
      return state
  }
}

export const getUserPositions = (state: IUserState) => state.positionsList
