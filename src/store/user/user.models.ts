export interface IUserState {
  readonly [key: string]: any
  loading: boolean
  error: string
  positionsList: []
}
