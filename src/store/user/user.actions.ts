import { action } from 'typesafe-actions'

export enum ActionTypes {
  GET_USER_POSITIONS_ACTION = 'GET_USER_POSITIONS_ACTION',
  GET_USER_POSITIONS_ACTION_SUCCESS = 'GET_USER_POSITIONS_ACTION_SUCCESS',
  GET_USER_POSITIONS_ACTION_ERROR = 'GET_USER_POSITIONS_ACTION_ERROR',
}

const GetUserPositionsAction = (payload?: any) => {
  return action(ActionTypes.GET_USER_POSITIONS_ACTION, payload)
}

const GetUserPositionsActionSuccess = (payload?: any) => {
  return action(ActionTypes.GET_USER_POSITIONS_ACTION_SUCCESS, payload)
}

const GetUserPositionsActionFail = (payload?: any) => {
  return action(ActionTypes.GET_USER_POSITIONS_ACTION_ERROR, payload)
}

export {
  GetUserPositionsAction,
  GetUserPositionsActionSuccess,
  GetUserPositionsActionFail,
}
