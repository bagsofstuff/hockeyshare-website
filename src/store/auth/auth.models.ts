export interface IAuthState {
  readonly [key: string]: any
  loading: any
  error: any
  userExists: boolean
  registerRedirect: boolean
  successForgotPassword: boolean
  resetPasswordSuccess: boolean
}
