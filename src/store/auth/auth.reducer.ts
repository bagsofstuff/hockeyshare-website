import { ActionTypes } from './auth.actions'
import { IAuthState } from '@store/auth/auth.models'

export const initialState: IAuthState = {
  loading: null,
  error: [],
  userExists: false,
  registerRedirect: false,
  successForgotPassword: false,
  resetPasswordSuccess: false,
}

export function reducer(
  state: IAuthState = initialState,
  action: any
): IAuthState {
  switch (action.type) {
    case ActionTypes.LOGIN_ACTION:
      return { ...state, loading: true, error: [] }
    case ActionTypes.LOGIN_ACTION_SUCCESS:
      return { ...state, loading: false, error: [] }
    case ActionTypes.LOGIN_ACTION_ERROR:
      return { ...state, loading: false, error: action.payload.errors }

    case ActionTypes.REGISTER_ACTION:
      return { ...state, loading: true, error: null }
    case ActionTypes.REGISTER_ACTION_SUCCESS:
      return { ...state, error: [], loading: false, registerRedirect: true }
    case ActionTypes.REGISTER_ACTION_ERROR:
      return { ...state, loading: false, error: action.payload.errors }

    case ActionTypes.CHECK_USERNAME_ACTION:
      return { ...state, loading: true, error: [], userExists: false }
    case ActionTypes.CHECK_USERNAME_ACTION_SUCCESS:
      return { ...state, loading: false, userExists: action.payload }
    case ActionTypes.CHECK_USERNAME_ACTION_ERROR:
      return { ...state, loading: false, userExists: false }

    case ActionTypes.EMAIL_VERIFY_ACTION:
      return { ...state, loading: true, error: [] }
    case ActionTypes.EMAIL_VERIFY_ACTION_SUCCESS:
      return { ...state, loading: false }
    case ActionTypes.EMAIL_VERIFY_ACTION_ERROR:
      return { ...state, loading: false }

    case ActionTypes.FORGOT_PASSWORD_ACTION:
      return { ...state, loading: true, error: [] }
    case ActionTypes.FORGOT_PASSWORD_ACTION_SUCCESS:
      return {
        ...state,
        loading: false,
        successForgotPassword: true,
        error: [],
      }
    case ActionTypes.FORGOT_PASSWORD_ACTION_ERROR:
      return { ...state, loading: false, error: action.payload.errors }

    case ActionTypes.RESET_PASSWORD_ACTION:
      return { ...state, loading: true, error: [] }
    case ActionTypes.RESET_PASSWORD_ACTION_SUCCESS:
      return { ...state, loading: false, resetPasswordSuccess: true, error: [] }
    case ActionTypes.RESET_PASSWORD_ACTION_ERROR:
      return { ...state, loading: false, error: action.payload.errors }

    default:
      return state
  }
}

export const getLoadingStatus = (state: IAuthState) => state.loading
export const getErrorsData = (state: IAuthState) => state.error
export const getUserExistsStatus = (state: IAuthState) => state.userExists
export const getRegisterRedirectStatus = (state: IAuthState) =>
  state.registerRedirect
export const getSuccessForgotPasswordStatus = (state: IAuthState) =>
  state.successForgotPassword
export const getSuccessResetPasswordStatus = (state: IAuthState) =>
  state.resetPasswordSuccess
