import { action } from 'typesafe-actions'

export enum ActionTypes {
  LOGIN_ACTION = 'LOGIN_ACTION',
  LOGIN_ACTION_SUCCESS = 'LOGIN_ACTION_SUCCESS',
  LOGIN_ACTION_ERROR = 'LOGIN_ACTION_ERROR',

  REGISTER_ACTION = 'REGISTER_ACTION',
  REGISTER_ACTION_SUCCESS = 'REGISTER_ACTION_SUCCESS',
  REGISTER_ACTION_ERROR = 'REGISTER_ACTION_ERROR',

  LOGOUT_ACTION = 'LOGOUT_ACTION',
  LOGOUT_ACTION_SUCCESS = 'LOGOUT_ACTION_SUCCESS',
  LOGOUT_ACTION_ERROR = 'LOGOUT_ACTION_ERROR',

  FORGOT_PASSWORD_ACTION = 'FORGOT_PASSWORD_ACTION',
  FORGOT_PASSWORD_ACTION_SUCCESS = 'FORGOT_PASSWORD_ACTION_SUCCESS',
  FORGOT_PASSWORD_ACTION_ERROR = 'FORGOT_PASSWORD_ACTION_ERROR',

  RESET_PASSWORD_ACTION = 'RESET_PASSWORD_ACTION',
  RESET_PASSWORD_ACTION_SUCCESS = 'RESET_PASSWORD_ACTION_SUCCESS',
  RESET_PASSWORD_ACTION_ERROR = 'RESET_PASSWORD_ACTION_ERROR',

  CHECK_USERNAME_ACTION = 'CHECK_USERNAME_ACTION',
  CHECK_USERNAME_ACTION_SUCCESS = 'CHECK_USERNAME_ACTION_SUCCESS',
  CHECK_USERNAME_ACTION_ERROR = 'CHECK_USERNAME_ACTION_ERROR',

  EMAIL_VERIFY_ACTION = 'EMAIL_VERIFY_ACTION',
  EMAIL_VERIFY_ACTION_SUCCESS = 'EMAIL_VERIFY_ACTION_SUCCESS',
  EMAIL_VERIFY_ACTION_ERROR = 'EMAIL_VERIFY_ACTION_ERROR',
}

const LoginAction = (payload?: any) => {
  return action(ActionTypes.LOGIN_ACTION, payload)
}

const LoginActionSuccess = (payload?: any) => {
  return action(ActionTypes.LOGIN_ACTION_SUCCESS, payload)
}

const LoginActionFail = (payload?: any) => {
  return action(ActionTypes.LOGIN_ACTION_ERROR, payload)
}

const RegisterAction = (payload: any) => {
  return action(ActionTypes.REGISTER_ACTION, payload)
}

const RegisterActionSuccess = (payload?: any) => {
  return action(ActionTypes.REGISTER_ACTION_SUCCESS, payload)
}

const RegisterActionFail = (payload?: any) => {
  return action(ActionTypes.REGISTER_ACTION_ERROR, payload)
}

const LogoutAction = (payload?: any) => {
  return action(ActionTypes.LOGOUT_ACTION, payload)
}

const LogoutActionSuccess = (payload?: any) => {
  return action(ActionTypes.LOGOUT_ACTION_SUCCESS, payload)
}

const LogoutActionFail = (payload?: any) => {
  return action(ActionTypes.LOGOUT_ACTION_ERROR, payload)
}

const ForgotPasswordAction = (payload?: any) => {
  return action(ActionTypes.FORGOT_PASSWORD_ACTION, payload)
}

const ForgotPasswordActionSuccess = (payload?: any) => {
  return action(ActionTypes.FORGOT_PASSWORD_ACTION_SUCCESS, payload)
}

const ForgotPasswordActionFail = (payload?: any) => {
  return action(ActionTypes.FORGOT_PASSWORD_ACTION_ERROR, payload)
}

const ResetPasswordAction = (payload?: any) => {
  return action(ActionTypes.RESET_PASSWORD_ACTION, payload)
}

const ResetPasswordActionSuccess = (payload?: any) => {
  return action(ActionTypes.RESET_PASSWORD_ACTION_SUCCESS, payload)
}

const ResetPasswordActionFail = (payload?: any) => {
  return action(ActionTypes.RESET_PASSWORD_ACTION_ERROR, payload)
}

const CheckUsernameAction = (payload?: any) => {
  return action(ActionTypes.CHECK_USERNAME_ACTION, payload)
}

const CheckUsernameActionSuccess = (payload?: any) => {
  return action(ActionTypes.CHECK_USERNAME_ACTION_SUCCESS, payload)
}

const CheckUsernameActionFail = (payload?: any) => {
  return action(ActionTypes.CHECK_USERNAME_ACTION_ERROR, payload)
}

const EmailVerifyAction = (payload?: any) => {
  return action(ActionTypes.EMAIL_VERIFY_ACTION, payload)
}

const EmailVerifyActionSuccess = (payload?: any) => {
  return action(ActionTypes.EMAIL_VERIFY_ACTION_SUCCESS, payload)
}

const EmailVerifyActionSuccessFail = (payload?: any) => {
  return action(ActionTypes.EMAIL_VERIFY_ACTION_ERROR, payload)
}

export {
  LoginAction,
  LoginActionSuccess,
  LoginActionFail,
  RegisterAction,
  RegisterActionSuccess,
  RegisterActionFail,
  LogoutAction,
  LogoutActionSuccess,
  LogoutActionFail,
  ForgotPasswordAction,
  ForgotPasswordActionSuccess,
  ForgotPasswordActionFail,
  ResetPasswordAction,
  ResetPasswordActionSuccess,
  ResetPasswordActionFail,
  CheckUsernameAction,
  CheckUsernameActionSuccess,
  CheckUsernameActionFail,
  EmailVerifyAction,
  EmailVerifyActionSuccess,
  EmailVerifyActionSuccessFail,
}
