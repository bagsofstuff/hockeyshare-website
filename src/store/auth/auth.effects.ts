import { put, takeLatest } from '@redux-saga/core/effects'
import {
  ActionTypes,
  RegisterActionFail,
  CheckUsernameActionSuccess,
  CheckUsernameActionFail,
  EmailVerifyActionSuccess,
  EmailVerifyActionSuccessFail,
  RegisterActionSuccess,
  ForgotPasswordActionSuccess,
  ForgotPasswordActionFail,
  ResetPasswordActionSuccess,
  ResetPasswordActionFail,
} from './auth.actions'
import { addToken, post } from '../../utils/httpRequests'
import Cookies from 'js-cookie'
import Router from 'next/router'

function* loginSaga(action: any) {
  try {
    const { data } = yield post('/api/v1/auth/login', action.payload)
    const cookieLeftTime = new Date(
      new Date().getTime() + data.data.expires_in * 1000
    )
    Cookies.set('token', data.data.access_token, { expires: cookieLeftTime })
    const token = Cookies.get('token')
    addToken(token)
    Router.push('/')
    yield put({ type: ActionTypes.LOGIN_ACTION_SUCCESS, payload: '777777' })
  } catch (error) {
    console.log('err')
    yield put({
      type: ActionTypes.LOGIN_ACTION_ERROR,
      payload: error.response.data,
    })
  }
}

function* registerSaga(action: any) {
  try {
    const { data } = yield post('/api/v1/auth/register', action.payload)
    if (data.data) {
      yield put(RegisterActionSuccess())
    }
  } catch (error) {
    yield put(RegisterActionFail(error.response.data))
  }
}

function* checkUsernameSaga(action: any) {
  try {
    const obj = {
      username: action.payload,
    }
    const { data } = yield post('/api/v1/users/username-exists', obj)

    yield put(CheckUsernameActionSuccess(data.data.exists))
  } catch (error) {
    yield put(CheckUsernameActionFail(error.response.data))
  }
}

function* emailVerifySaga(action: any) {
  try {
    const obj = {
      token: action.payload,
    }
    yield post('/api/v1/auth/email/verify', obj)
    yield put(EmailVerifyActionSuccess())
  } catch (error) {
    yield put(EmailVerifyActionSuccessFail(error.response.data))
  }
}

function* forgotPasswordSaga(action: any) {
  try {
    const obj = {
      email: action.payload,
    }
    yield post('/api/v1/auth/password/email', obj)
    yield put(ForgotPasswordActionSuccess())
  } catch (error) {
    yield put(ForgotPasswordActionFail(error.response.data))
  }
}

function* resetPasswordSaga(action: any) {
  try {
    yield post('/api/v1/auth/password/reset', action.payload)
    yield put(ResetPasswordActionSuccess())
  } catch (error) {
    yield put(ResetPasswordActionFail(error.response.data))
  }
}

export function* authSaga() {
  yield takeLatest(ActionTypes.LOGIN_ACTION, loginSaga)
  yield takeLatest(ActionTypes.REGISTER_ACTION, registerSaga)
  yield takeLatest(ActionTypes.CHECK_USERNAME_ACTION, checkUsernameSaga)
  yield takeLatest(ActionTypes.EMAIL_VERIFY_ACTION, emailVerifySaga)
  yield takeLatest(ActionTypes.FORGOT_PASSWORD_ACTION, forgotPasswordSaga)
  yield takeLatest(ActionTypes.RESET_PASSWORD_ACTION, resetPasswordSaga)
}
