import { combineReducers, Reducer } from 'redux'
import { createSelector } from 'reselect'

import * as authReducer from './auth/auth.reducer'
import * as userReducer from './user/user.reducer'
import * as modalsReducer from './modals/modals.reducer'
import { IAuthState } from '@store/auth/auth.models'
import { IUserState } from '@store/user/user.models'
import { IModalsState } from '@store/modals/modals.models'

export interface IApplicationState {
  readonly auth: IAuthState
  readonly user: IUserState
  readonly modals: IModalsState
}

const rootReducer: Reducer<IApplicationState> = combineReducers<
  IApplicationState
>({
  auth: authReducer.reducer,
  user: userReducer.reducer,
  modals: modalsReducer.reducer,
})

export default rootReducer

// selectors

// IAuthState

export const getAuthState = (state: IApplicationState): any => state.auth

export const selectLoadingStatus = createSelector(
  getAuthState,
  authReducer.getLoadingStatus
)

export const selectRegisterRedirectStatus = createSelector(
  getAuthState,
  authReducer.getRegisterRedirectStatus
)

export const selectUserExistsStatus = createSelector(
  getAuthState,
  authReducer.getUserExistsStatus
)

export const selectErrorsData = createSelector(
  getAuthState,
  authReducer.getErrorsData
)

export const selectForgotPasswordStatus = createSelector(
  getAuthState,
  authReducer.getSuccessForgotPasswordStatus
)

export const selectResetPasswordStatus = createSelector(
  getAuthState,
  authReducer.getSuccessResetPasswordStatus
)

// IUserState
export const getUserState = (state: IApplicationState): any => state.user

export const selectUserPositions = createSelector(
  getUserState,
  userReducer.getUserPositions
)

// IModalState

export const getModalsState = (state: IApplicationState): IModalsState =>
  state.modals

export const selectActiveModals = createSelector(
  getModalsState,
  modalsReducer.getActiveModals
)
