import { ActionTypes } from './modals.actions'
import { IModalsState } from './modals.models'

const initialState: IModalsState = {
  activeModals: [],
}

export function reducer(
  state: IModalsState = initialState,
  action: any
): IModalsState {
  switch (action.type) {
    case ActionTypes.OPEN_MODAL_ACTION:
      return {
        ...state,
        activeModals: [...state.activeModals, action.payload.name],
      }
    case ActionTypes.CLOSE_MODAL_ACTION:
      return {
        ...state,
        activeModals: state.activeModals.filter(v => v !== action.payload.name),
      }

    default:
      return state
  }
}

// selectors
export const getActiveModals = (state: IModalsState) => state.activeModals
