import { action } from 'typesafe-actions'
import { IModal } from './modals.models'

export enum ActionTypes {
  OPEN_MODAL_ACTION = 'OPEN_MODAL_ACTION',
  CLOSE_MODAL_ACTION = 'CLOSE_MODAL_ACTION',
}

const OpenModalAction = (payload: IModal) => {
  return action(ActionTypes.OPEN_MODAL_ACTION, payload)
}

const CloseModalAction = (payload: IModal) => {
  return action(ActionTypes.CLOSE_MODAL_ACTION, payload)
}

export { OpenModalAction, CloseModalAction }
