export interface IModalsState {
  readonly [key: string]: any
  readonly activeModals: string[]
}

export interface IModal {
  readonly name: string
  readonly data?: any
}
