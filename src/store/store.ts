import { createStore, applyMiddleware, Middleware, Store } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import rootReducer, { IApplicationState } from './root.reducer'
import rootSaga from './root.effects'

const sagaMiddleware = createSagaMiddleware()

let middleware: Middleware[] = [sagaMiddleware]

if (process.env.NODE_ENV !== 'production') {
  const createLogger = require('redux-logger').createLogger
  const loggerMiddleware = createLogger({ collapsed: true, duration: true })
  middleware = [...middleware, loggerMiddleware]
}

export const initStore = (initialState = {}): Store<IApplicationState> => {
  const store: any = createStore(
    rootReducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middleware))
  )
  store.sagaTask = sagaMiddleware.run(rootSaga)
  return store
}
