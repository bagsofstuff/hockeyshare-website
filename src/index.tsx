import React, { Component } from 'react'
import { END } from 'redux-saga'
import { parseCookies } from './utils/parseCookies'

function withReduxSaga(BaseComponent: any) {
  class WrappedComponent extends Component {
    public static displayName = `withReduxSaga(${BaseComponent.displayName ||
      BaseComponent.name ||
      'BaseComponent'})`

    public static async getInitialProps(props: any) {
      const { isServer, store, req } = props
      const cookie = parseCookies(req)

      let pageProps = {}

      if (BaseComponent.getInitialProps) {
        pageProps = await BaseComponent.getInitialProps(props)
      }

      // Stop saga on the server
      if (isServer) {
        store.dispatch(END)
        await store.sagaTask.toPromise()
      }

      const { token } = cookie

      pageProps = { ...pageProps, token }

      return pageProps
    } // end getInitialProps

    public render() {
      return <BaseComponent {...this.props} />
    }
  } // end class

  return WrappedComponent
}

export default withReduxSaga
