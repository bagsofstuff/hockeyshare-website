import React from 'react'
import Header from '@components/Header/Header'
import Footer from '@components/Footer/Footer'
import TopBanner from '@containers/HomePage/components/TopBanner/TopBanner'
import withReduxSaga from '../src'

const HomePage = () => {
  return (
    <main>
      <Header />
      <div className="wrapper">
        <TopBanner />
      </div>
      <Footer />
    </main>
  )
}

export default withReduxSaga(HomePage)
