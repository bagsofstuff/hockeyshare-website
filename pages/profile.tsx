import React from 'react'
import Header from '@components/Header/Header'
import Footer from '@components/Footer/Footer'
import Router from 'next/router'
import { parseCookies } from '../src/utils/parseCookies'
import withReduxSaga from '../src'

const ProfilePage = () => {
  return (
    <main>
      <Header />
      <div className="content-wrapper">
        <h1>Profile page</h1>
      </div>
      <Footer />
    </main>
  )
}

ProfilePage.getInitialProps = (props: any) => {
  const { isServer, req, res } = props
  const cookie = parseCookies(req)
  const { token } = cookie

  if (!token) {
    if (isServer) {
      res.writeHead(302, { Location: '/auth/login' })
      res.end()
      return
    } else {
      Router.push('/auth/login')
    }
  }

  return { isServer }
}

export default withReduxSaga(ProfilePage)
