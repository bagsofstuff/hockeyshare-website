import React from 'react'
import Header from '@components/Header/Header'
import Footer from '@components/Footer/Footer'
import Recovery from '@containers/Auth/Recovery/Recovery'

const RecoveryPage = () => {
  return (
    <main>
      <Header />
      <div className="content-wrapper">
        <Recovery />
      </div>
      <Footer />
    </main>
  )
}

export default RecoveryPage
