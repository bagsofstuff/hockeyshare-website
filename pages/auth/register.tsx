import React from 'react'

import Footer from '@components/Footer/Footer'
import Header from '@components/Header/Header'
import withReduxSaga from '../../src'
import RegisterContainer from '@containers/Auth/Register/RegisterContainer'
import { parseCookies } from '../../src/utils/parseCookies'
import Router from 'next/router'

const RegisterPage = () => {
  return (
    <main>
      <Header />
      <div className="content-wrapper">
        <RegisterContainer />
      </div>
      <Footer />
    </main>
  )
}

RegisterPage.getInitialProps = (props: any) => {
  const { isServer, req, res } = props
  const cookie = parseCookies(req)
  const { token } = cookie

  if (token) {
    if (isServer) {
      res.writeHead(302, { Location: '/' })
      res.end()
      return
    } else {
      Router.push('/')
    }
  }

  return { isServer }
}

export default withReduxSaga(RegisterPage)
