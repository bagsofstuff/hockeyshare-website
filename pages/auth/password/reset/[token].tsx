import React from 'react'
import Header from '@components/Header/Header'
import Footer from '@components/Footer/Footer'
import ResetPassword from '@containers/Auth/ResetPassword/ResetPassword'

const ResetPasswordPage = () => {
  return (
    <main>
      <Header />
      <div className="content-wrapper">
        <ResetPassword />
      </div>
      <Footer />
    </main>
  )
}

export default ResetPasswordPage
