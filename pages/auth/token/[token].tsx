import React from 'react'
import Header from '@components/Header/Header'
import Footer from '@components/Footer/Footer'
import Token from '@containers/Auth/Token/Token'

const TokenPage = () => {
  return (
    <main>
      <Header />
      <div className="content-wrapper">
        <Token />
      </div>
      <Footer />
    </main>
  )
}

export default TokenPage
