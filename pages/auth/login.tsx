import React from 'react'
import Login from '@containers/Auth/Login/Login'
import Header from '@components/Header/Header'
import Footer from '@components/Footer/Footer'
import Router from 'next/router'
import withReduxSaga from '../../src'
import { parseCookies } from '../../src/utils/parseCookies'

const LoginPage = () => {
  return (
    <main>
      <Header />
      <div className="content-wrapper">
        <Login />
      </div>
      <Footer />
    </main>
  )
}

LoginPage.getInitialProps = (props: any) => {
  const { isServer, req, res } = props
  const cookie = parseCookies(req)
  const { token } = cookie

  if (token) {
    if (isServer) {
      res.writeHead(302, { Location: '/' })
      res.end()
      return
    } else {
      Router.push('/')
    }
  }

  return { isServer }
}

export default withReduxSaga(LoginPage)
